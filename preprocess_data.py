# data using is: Berkeley DeepDrive 100k
import os
import cv2
import shutil
import json
import numpy as np

from constant import *


def pre_process_images(in_dir, out_dir):
    print("Pre-processing images...")
    # make out_dir
    if os.path.exists(out_dir):
        shutil.rmtree(out_dir)
    os.mkdir(out_dir)
    for d in os.listdir(in_dir):
        os.mkdir(os.path.join(out_dir, d))

    # pre-process images
    for d in os.listdir(in_dir):
        files = os.listdir(os.path.join(in_dir, d))
        for f in files:
            print("Pre-process Image: ", f)
            image = cv2.imread(os.path.join(in_dir, d, f))
            image = cv2.resize(image, (IMAGE_W, IMAGE_H))
            cv2.imwrite(os.path.join(out_dir, d, f), image)


def get_grid_cell(bbox):
    x_center = bbox[0] + bbox[2] / 2
    y_center = bbox[1] + bbox[3] / 2
    grid_x = int(x_center / IMAGE_W * GRID_W)
    grid_y = int(y_center / IMAGE_H * GRID_H)

    return grid_x, grid_y


def iou_wh(r1, r2):
    min_w = min(r1[0], r2[0])
    min_h = min(r1[1], r2[1])
    area_r1 = r1[0] * r1[1]
    area_r2 = r2[0] * r2[1]

    intersect = min_w * min_h
    union = area_r1 + area_r2 - intersect
    iou = intersect / union

    return iou


def get_active_anchor(bbox):
    indexs = []
    iou_max = 0
    index_max = 0
    for i, a in enumerate(ANCHORS):
        iou = iou_wh(bbox[2:], a)

        if iou > IOU_THRES:
            indexs.append(i)

        if iou > iou_max:
            iou_max = iou
            index_max = i

    if len(indexs) == 0:
        indexs.append(index_max)

    return indexs


def bbox_to_label(bbox, anchor):
    x_center = bbox[0] + bbox[2] / 2
    y_center = bbox[1] + bbox[3] / 2
    grid_x = x_center / IMAGE_W * GRID_W
    grid_y = y_center / IMAGE_H * GRID_H

    grid_x_offset = grid_x - int(grid_x)
    grid_y_offset = grid_y - int(grid_y)
    bbox_w_scale = bbox[2] / anchor[0]
    bbox_h_scale = bbox[3] / anchor[1]
    label = [grid_x_offset, grid_y_offset, bbox_w_scale, bbox_h_scale]

    return label


def pre_process_labels(in_dir, out_dir):
    print("Pre-processing labels...")
    # make out_dir
    dirs = ['train', 'val']
    if os.path.exists(out_dir):
        shutil.rmtree(out_dir)
    os.mkdir(out_dir)
    for d in dirs:
        os.mkdir(os.path.join(out_dir, d))

    # pre-process label
    for f in os.listdir(in_dir):
        f_path = os.path.join(in_dir, f)

        with open(f_path) as file:
            data = json.load(file)

        for d in data:
            name = d['name']
            print("Pre-process Label: ", name)
            label = np.zeros([GRID_H, GRID_W, NUM_ANCHOR, 6], dtype=np.float32)

            labels = d['labels']
            for l in labels:
                if 'box2d' in l:
                    c = l['category']
                    cls = CLASSES[c]

                    bbox = l['box2d']
                    x1 = bbox['x1']
                    y1 = bbox['y1']
                    x2 = bbox['x2']
                    y2 = bbox['y2']
                    x1 = x1 / ORG_IMAGE_W * IMAGE_W
                    y1 = y1 / ORG_IMAGE_H * IMAGE_H
                    x2 = x2 / ORG_IMAGE_W * IMAGE_W
                    y2 = y2 / ORG_IMAGE_H * IMAGE_H
                    bbox = [x1, y1, x2 - x1, y2 - y1]

                    grid_x, grid_y = get_grid_cell(bbox)
                    active_index_anchors = get_active_anchor(bbox)

                    for index in active_index_anchors:
                        anchor_label = bbox_to_label(bbox, ANCHORS[index])
                        label[grid_y, grid_x, index] = np.concatenate((anchor_label, [cls], [1.0]))

            # write label to file npy
            label_file_name = os.path.splitext(name)[0] + '.npy'
            if 'train' in f:
                out_path = os.path.join(out_dir, 'train', label_file_name)
            else:
                out_path = os.path.join(out_dir, 'val', label_file_name)

            np.save(out_path, label)


def main():
    images_dir = "D:/data/bdd100k/images/100k"
    out_images_dir = "D:/data/bdd100k/preprocess_images"
    labels_dir = "D:/data/bdd100k/labels"
    out_labels_dir = "D:/data/bdd100k/preprocess_labels"

    pre_process_images(images_dir, out_images_dir)
    pre_process_labels(labels_dir, out_labels_dir)

    print('Done !')


if __name__ == '__main__':
    main()
