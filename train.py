import os

from model import YOLO


def main():
    train_image_dir = "D:/longpv/data/bdd100k/preprocess_images/train"
    train_label_dir = "D:/longpv/data/bdd100k/preprocess_labels/train"
    val_image_dir = "D:/longpv/data/bdd100k/preprocess_images/val"
    val_label_dir = "D:/longpv/data/bdd100k/preprocess_labels/val"

    train_label_paths = [os.path.join(train_label_dir, f) for f in os.listdir(train_label_dir)]
    train_image_paths = [os.path.join(train_image_dir, os.path.splitext(f)[0] + '.jpg')
                         for f in os.listdir(train_label_dir)]

    val_image_paths = [os.path.join(val_image_dir, f) for f in os.listdir(val_image_dir)]
    val_label_paths = [os.path.join(val_label_dir, f) for f in os.listdir(val_label_dir)]

    train_from_scratch = True

    # train model
    model = YOLO()
    if not train_from_scratch:
        model.restore_weight()

    model.train(train_image_paths, train_label_paths, val_image_paths, val_label_paths, num_epoch=50, batch_size=10)


if __name__ == '__main__':
    main()
