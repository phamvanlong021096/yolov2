import os
import cv2
import numpy as np
import tensorflow as tf

from constant import *
from utils import shuffle_data, read_images, read_labels, read_image_predict, softmax, sigmoid, iou


class YOLO(object):

    def __init__(self):
        print("Building model...")
        self.X = tf.placeholder(shape=[None, IMAGE_H, IMAGE_W, IMAGE_C], dtype=tf.float32, name='input_image')
        self.Y = tf.placeholder(shape=[None, GRID_H, GRID_W, NUM_ANCHOR, 6], dtype=tf.float32, name='label')
        self.train_flag = tf.placeholder(dtype=tf.bool, name='train_flag')

        # build model
        with tf.variable_scope('net'):
            self.logits = self.__yolo_net(self.X, self.train_flag)
        with tf.name_scope('loss'):
            self.loss = self.__yolo_loss(self.logits, self.Y)

        self.opt = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE).minimize(self.loss)
        self.saver = tf.train.Saver()
        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())
        print("Model initialized.")

    def __yolo_net(self, x, train):
        x = self.__conv_layer_bn(x, (3, 3), 32, train, 'conv1')
        x = self.__maxpool_layer(x, (2, 2), (2, 2), 'maxpool1')
        x = self.__conv_layer_bn(x, (3, 3), 64, train, 'conv2')
        x = self.__maxpool_layer(x, (2, 2), (2, 2), 'maxpool2')

        x = self.__conv_layer_bn(x, (3, 3), 128, train, 'conv3')
        x = self.__conv_layer_bn(x, (1, 1), 64, train, 'conv4')
        x = self.__conv_layer_bn(x, (3, 3), 128, train, 'conv5')
        x = self.__maxpool_layer(x, (2, 2), (2, 2), 'maxpool5')

        x = self.__conv_layer_bn(x, (3, 3), 256, train, 'conv6')
        x = self.__conv_layer_bn(x, (1, 1), 128, train, 'conv7')
        x = self.__conv_layer_bn(x, (3, 3), 256, train, 'conv8')
        x = self.__maxpool_layer(x, (2, 2), (2, 2), 'maxpool8')

        x = self.__conv_layer_bn(x, (3, 3), 512, train, 'conv9')
        x = self.__conv_layer_bn(x, (1, 1), 256, train, 'conv10')
        x = self.__conv_layer_bn(x, (3, 3), 512, train, 'conv11')
        x = self.__conv_layer_bn(x, (1, 1), 256, train, 'conv12')
        passthrough = self.__conv_layer_bn(x, (3, 3), 512, train, 'conv13')
        x = self.__maxpool_layer(passthrough, (2, 2), (2, 2), 'maxpool13')

        x = self.__conv_layer_bn(x, (3, 3), 1024, train, 'conv14')
        x = self.__conv_layer_bn(x, (1, 1), 512, train, 'conv15')
        x = self.__conv_layer_bn(x, (3, 3), 1024, train, 'conv16')
        x = self.__conv_layer_bn(x, (1, 1), 512, train, 'conv17')
        x = self.__conv_layer_bn(x, (3, 3), 1024, train, 'conv18')

        x = self.__conv_layer_bn(x, (3, 3), 1024, train, 'conv19')
        x = self.__conv_layer_bn(x, (3, 3), 1024, train, 'conv20')
        x = self.__passthrough_layer(x, passthrough, (3, 3), 64, 2, train, 'conv21')
        x = self.__conv_layer_bn(x, (3, 3), 1024, train, 'conv22')
        x = self.__conv_layer(x, (1, 1), NUM_ANCHOR * (NUM_CLASS + 5), 'conv23')

        y = tf.reshape(x, shape=(-1, GRID_H, GRID_W, NUM_ANCHOR, NUM_CLASS + 5), name='logits')

        return y

    def __conv_layer_bn(self, x, kernel, depth, train, name):
        with tf.variable_scope(name):
            x = tf.layers.conv2d(x, depth, kernel, padding='SAME',
                                 kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                 bias_initializer=tf.zeros_initializer())
            x = tf.layers.batch_normalization(x, training=train, momentum=0.99, epsilon=0.001, center=True, scale=True)
            # Leaky ReLU
            x = tf.maximum(x, ALPHA * x)

        return x

    def __conv_layer(self, x, kernel, depth, name):
        with tf.variable_scope(name):
            x = tf.layers.conv2d(x, depth, kernel, padding='SAME',
                                 kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                 bias_initializer=tf.zeros_initializer())

        return x

    def __maxpool_layer(self, x, size, stride, name):
        with tf.name_scope(name):
            x = tf.layers.max_pooling2d(x, size, stride, padding='SAME')

        return x

    def __passthrough_layer(self, a, b, kernel, depth, size, train, name):
        b = self.__conv_layer_bn(b, kernel, depth, train, name)
        b = tf.space_to_depth(b, size)
        y = tf.concat([a, b], axis=3)

        return y

    def __yolo_loss(self, logits, labels):
        mask = self.__slice_tensor(labels, 5)
        labels = self.__slice_tensor(labels, 0, 4)

        mask = tf.cast(tf.reshape(mask, shape=(-1, GRID_H, GRID_W, NUM_ANCHOR)), tf.bool)

        with tf.name_scope('mask'):
            masked_labels = tf.boolean_mask(labels, mask)
            masked_logits = tf.boolean_mask(logits, mask)
            neg_masked_logits = tf.boolean_mask(logits, tf.logical_not(mask))

        with tf.name_scope('trans_logits'):
            masked_logits_xy = tf.sigmoid(self.__slice_tensor(masked_logits, 0, 1))
            masked_logits_wh = tf.exp(self.__slice_tensor(masked_logits, 2, 3))
            masked_logits_o = tf.sigmoid(self.__slice_tensor(masked_logits, 4))
            masked_logits_no_o = tf.sigmoid(self.__slice_tensor(neg_masked_logits, 4))
            masked_logits_c = tf.nn.softmax(self.__slice_tensor(masked_logits, 5, -1))

        with tf.name_scope('trans_labels'):
            masked_label_xy = self.__slice_tensor(masked_labels, 0, 1)
            masked_label_wh = self.__slice_tensor(masked_labels, 2, 3)
            masked_label_c = self.__slice_tensor(masked_labels, 4)
            masked_label_c_vec = tf.reshape(
                tf.one_hot(tf.cast(masked_label_c, tf.int32), depth=NUM_CLASS), shape=(-1, NUM_CLASS))

        with tf.name_scope('merge'):
            with tf.name_scope('loss_xy'):
                loss_xy = tf.reduce_sum(tf.square(masked_logits_xy - masked_label_xy))
            with tf.name_scope('loss_wh'):
                loss_wh = tf.reduce_sum(tf.square(tf.sqrt(masked_logits_wh) - tf.sqrt(masked_label_wh)))
            with tf.name_scope('loss_obj'):
                loss_obj = tf.reduce_sum(tf.square(masked_logits_o - 1))
            with tf.name_scope('loss_no_obj'):
                loss_no_obj = tf.reduce_sum(tf.square(masked_logits_no_o))
            with tf.name_scope('loss_class'):
                loss_c = tf.reduce_sum(tf.square(masked_logits_c - masked_label_c_vec))

            loss = LAMBDA_COORD * (loss_xy + loss_wh) + loss_obj + LAMBDA_NO_OBJ * loss_no_obj + loss_c

        return loss

    def __slice_tensor(self, x, start_index, end_index=None):
        if end_index is not None and end_index < 0:
            y = x[..., start_index:]
        else:
            if end_index is None:
                end_index = start_index
            y = x[..., start_index: end_index + 1]

        return y

    def train(self, train_image_paths, train_label_paths, val_image_paths, val_label_paths,
              num_epoch, batch_size, save_model_after=5):
        print("Start training...")
        num_train_image = len(train_image_paths)
        num_val_image = len(val_image_paths)
        num_batch_train = (num_train_image - 1) // batch_size + 1
        num_batch_val = (num_val_image - 1) // batch_size + 1

        for e in range(num_epoch):
            loss_train = 0
            loss_val = 0

            # shuffle data
            train_image_paths, train_label_paths = shuffle_data(train_image_paths, train_label_paths)

            # train
            for i in range(num_batch_train):
                start_index = i * batch_size
                end_index = min(start_index + batch_size, num_train_image)
                batch_X = read_images(train_image_paths[start_index: end_index])
                batch_Y = read_labels(train_label_paths[start_index: end_index])
                _, loss = self.sess.run([self.opt, self.loss],
                                        feed_dict={self.X: batch_X, self.Y: batch_Y, self.train_flag: True})
                loss_train += loss
            loss_train /= num_batch_train

            # valid
            for i in range(num_batch_val):
                start_index = i * batch_size
                end_index = min(start_index + batch_size, num_val_image)
                batch_X = read_images(val_image_paths[start_index: end_index])
                batch_Y = read_labels(val_label_paths[start_index: end_index])
                loss = self.sess.run(self.loss, feed_dict={self.X: batch_X, self.Y: batch_Y, self.train_flag: True})
                loss_val += loss
            loss_val /= num_batch_val

            print("Step: ", e)
            print("     Loss train: ", loss_train)
            print("     Loss valid: ", loss_val)

            if e % save_model_after == 0:
                self.__save_model()

        self.__save_model()
        print("Training done !")

    def eval(self, train_image_paths, train_label_paths, val_image_paths, val_label_paths, batch_size):
        print("Start eval...")
        num_train_image = len(train_image_paths)
        num_val_image = len(val_image_paths)
        num_batch_train = (num_train_image - 1) // batch_size + 1
        num_batch_val = (num_val_image - 1) // batch_size + 1

        loss_train = 0
        loss_val = 0

        # shuffle data
        train_image_paths, train_label_paths = shuffle_data(train_image_paths, train_label_paths)

        # train
        for i in range(num_batch_train):
            start_index = i * batch_size
            end_index = min(start_index + batch_size, num_train_image)
            batch_X = read_images(train_image_paths[start_index: end_index])
            batch_Y = read_labels(train_label_paths[start_index: end_index])
            loss = self.sess.run(self.loss, feed_dict={self.X: batch_X, self.Y: batch_Y, self.train_flag: True})
            loss_train += loss
        loss_train /= num_batch_train

        # valid
        for i in range(num_batch_val):
            start_index = i * batch_size
            end_index = min(start_index + batch_size, num_val_image)
            batch_X = read_images(val_image_paths[start_index: end_index])
            batch_Y = read_labels(val_label_paths[start_index: end_index])
            loss = self.sess.run(self.loss, feed_dict={self.X: batch_X, self.Y: batch_Y, self.train_flag: True})
            loss_val += loss
        loss_val /= num_batch_val

        print("     Loss train: ", loss_train)
        print("     Loss valid: ", loss_val)

    def __save_model(self):
        self.saver.save(self.sess, WEIGHT_PATH)

    def restore_weight(self):
        self.saver.restore(self.sess, WEIGHT_PATH)
        print("Model restored !")

    def restore_weight_test(self, f):
        self.saver.restore(self.sess, f)
        print("Model restored !")

    def predict_and_draw(self, image_path):
        image = read_image_predict(image_path)
        logits = self.sess.run(self.logits, feed_dict={self.X: image, self.train_flag: True})

        classes, rois = self.__preprocess_yolo_output(logits)
        classes, indexs = self.__non_max_supression(classes, rois)

        # draw object and save
        image = cv2.imread(image_path)
        image = self.__draw_objects(classes, rois, indexs, image)

        base = os.path.basename(image_path)
        out_path = os.path.join('./static/img', os.path.splitext(base)[0] + "_out.jpg")
        cv2.imwrite(out_path, image)
        return out_path

    def __preprocess_yolo_output(self, logits):
        locations = []
        classes = []

        for i in range(GRID_H):
            for j in range(GRID_W):
                for k in range(NUM_ANCHOR):
                    class_vec = softmax(logits[0, i, j, k, 5:])
                    objectness = sigmoid(logits[0, i, j, k, 4])

                    class_prob = objectness * class_vec

                    w = np.exp(logits[0, i, j, k, 2]) * ANCHORS[k][0]
                    h = np.exp(logits[0, i, j, k, 3]) * ANCHORS[k][1]
                    dx = sigmoid(logits[0, i, j, k, 0])
                    dy = sigmoid(logits[0, i, j, k, 1])
                    x = (j + dx) * SCALE - w / 2.0
                    y = (i + dy) * SCALE - h / 2.0

                    classes.append(class_prob)
                    locations.append([x, y, w, h])

        classes = np.array(classes)
        locations = np.array(locations)

        return classes, locations

    def __non_max_supression(self, classes, locations):
        classes = np.transpose(classes)
        indexs = np.argsort(-classes, axis=1)

        for i in range(classes.shape[0]):
            classes[i] = classes[i][indexs[i]]

        for class_idx, class_vec in enumerate(classes):
            for roi_idx, roi_prob in enumerate(class_vec):
                if roi_prob < PROB_TH:
                    classes[class_idx][roi_idx] = 0

        for class_idx, class_vec in enumerate(classes):
            for roi_idx, roi_prob in enumerate(class_vec):

                if roi_prob == 0:
                    continue

                roi = locations[indexs[class_idx][roi_idx]]

                for roi_ref_idx, roi_ref_prob in enumerate(class_vec):

                    if roi_ref_prob == 0 or roi_ref_idx <= roi_idx:
                        continue

                    roi_ref = locations[indexs[class_idx][roi_ref_idx]]

                    if iou(roi, roi_ref) > IOU_TH:
                        classes[class_idx][roi_ref_idx] = 0

        return classes, indexs

    def __draw_objects(self, classes, rois, indexs, img):
        scale_w = img.shape[1] / float(IMAGE_W)
        scale_h = img.shape[0] / float(IMAGE_H)

        for class_idx, class_ in enumerate(classes):
            for loc_idx, class_prob in enumerate(class_):
                if class_prob > 0:
                    x = int(rois[indexs[class_idx][loc_idx]][0] * scale_w)
                    y = int(rois[indexs[class_idx][loc_idx]][1] * scale_h)
                    w = int(rois[indexs[class_idx][loc_idx]][2] * scale_w)
                    h = int(rois[indexs[class_idx][loc_idx]][3] * scale_h)

                    cv2.rectangle(img, (x, y), (x + w, y + h), COLORS[class_idx], 2)
                    text = CATEGORIES[class_idx]
                    cv2.putText(img, text, (x, y - 8), cv2.FONT_HERSHEY_SIMPLEX, 0.6, COLORS[class_idx], 2, cv2.LINE_AA)
        return img

    def predict(self, image_path):
        res = []
        image = read_image_predict(image_path)
        logits = self.sess.run(self.logits, feed_dict={self.X: image, self.train_flag: True})

        classes, rois = self.__preprocess_yolo_output(logits)
        classes, indexs = self.__non_max_supression(classes, rois)

        image = cv2.imread(image_path)
        scale_w = image.shape[1] / float(IMAGE_W)
        scale_h = image.shape[0] / float(IMAGE_H)

        for class_idx, class_ in enumerate(classes):
            for loc_idx, class_prob in enumerate(class_):
                if class_prob > 0:
                    x = int(rois[indexs[class_idx][loc_idx]][0] * scale_w)
                    y = int(rois[indexs[class_idx][loc_idx]][1] * scale_h)
                    w = int(rois[indexs[class_idx][loc_idx]][2] * scale_w)
                    h = int(rois[indexs[class_idx][loc_idx]][3] * scale_h)

                    text = CATEGORIES[class_idx]
                    if text == 'car':
                        res.append((x, y, x + w, y + h))

        return res
