import os

from flask import Flask, render_template, request
from model import YOLO

app = Flask(__name__, static_folder="static")
UPLOAD_FOLDER = os.path.basename('img')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/')
def main_page():
    return render_template('index.html')


@app.route('/upload', methods=['POST'])
def detection():
    # save file image to img directory
    file = request.files['image']
    path = os.path.join('./static', app.config['UPLOAD_FOLDER'], file.filename)
    file.save(path)

    # detect
    link = model.predict_and_draw(path)

    return render_template('index.html', link=link)


if __name__ == '__main__':
    model = YOLO()
    model.restore_weight()
    app.run(host='0.0.0.0', port=5000)
