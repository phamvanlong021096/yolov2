import os
import cv2
import json

from model import YOLO


def read_labels(path):
    with open(path) as f:
        data = json.load(f)

    res = []
    objects = data['annotations']
    for obj in objects:
        if obj['class'] == 'car':
            x1 = obj['x']
            y1 = obj['y']
            x2 = x1 + obj['width']
            y2 = y1 + obj['height']
            res.append((x1, y1, x2, y2))

    return res


def cal_iou(r1, r2):
    intersect_w = max(min(r1[2], r2[2]) - max(r1[0], r2[0]), 0)
    intersect_h = max(min(r1[3], r2[3]) - max(r1[1], r2[1]), 0)
    area_r1 = (r1[2] - r1[0]) * (r1[3] - r1[1])
    area_r2 = (r2[2] - r2[0]) * (r2[3] - r2[1])
    intersect = intersect_w * intersect_h
    union = area_r1 + area_r2 - intersect

    return intersect / union


def write_to_csv(res):
    path = os.getcwd() + './compare.csv'
    if os.path.exists(path):
        os.remove(path)
    with open(path, 'a') as f:
        row = "image,x1_gt,y1_gt,x2_gt,y2_gt,x1_ai,y1_ai,x2_ai,y2_ai,iou\n"
        f.write(row)

        for r in res:
            row = r[0] + ',' + str(r[1][0]) + ',' + str(r[1][1]) + ',' + str(r[1][2]) + ',' + str(r[1][3]) + ',' \
                  + str(r[2][0]) + ',' + str(r[2][1]) + ',' + str(r[2][2]) + ',' + str(r[2][3]) + ',' + str(r[3]) + '\n'
            f.write(row)


def main():
    image_dir = "D:/data/fpt/Samples/Images"
    label_dir = "D:/data/fpt/Samples/Json"

    image_paths = [os.path.join(image_dir, f) for f in os.listdir(image_dir)]
    label_paths = [os.path.join(label_dir, f) for f in os.listdir(label_dir)]
    image_files = os.listdir(image_dir)
    n = len(image_paths)

    res = []
    model = YOLO()
    model.restore_weight()
    for k in range(n):
        preds = model.predict(image_paths[k])
        labels = read_labels(label_paths[k])

        color = [0 for i in range(len(preds))]

        for i, l in enumerate(labels):
            max_iou = 0
            index = -1
            for j, p in enumerate(preds):
                iou = cal_iou(l, p)
                if color[j] == 0 and iou > 0.5 and iou > max_iou:
                    max_iou = iou
                    index = j

            if index != -1:
                res.append([image_files[k], l, preds[index], max_iou])
                color[index] = 1

    write_to_csv(res)

    print(res)


if __name__ == '__main__':
    main()
