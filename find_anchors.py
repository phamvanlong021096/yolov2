import os
import json
import numpy as np

from sklearn.cluster import KMeans
from constant import *


def main():
    labels_dir = 'D:/data/bdd100k/labels'

    X = []
    for f in os.listdir(labels_dir):
        labels_path = os.path.join(labels_dir, f)

        with open(labels_path) as file:
            data = json.load(file)

        for d in data:
            labels = d['labels']
            for l in labels:
                if 'box2d' in l:
                    bbox = l['box2d']
                    x1 = bbox['x1']
                    y1 = bbox['y1']
                    x2 = bbox['x2']
                    y2 = bbox['y2']

                    x1 = x1 / ORG_IMAGE_W * IMAGE_W
                    y1 = y1 / ORG_IMAGE_H * IMAGE_H
                    x2 = x2 / ORG_IMAGE_W * IMAGE_W
                    y2 = y2 / ORG_IMAGE_H * IMAGE_H

                    a_w = x2 - x1
                    a_h = y2 - y1
                    X.append([a_w, a_h])

    X = np.array(X, dtype=np.float32)
    # print(X)

    kmeans = KMeans(n_clusters=NUM_ANCHOR, random_state=0).fit(X)
    print("Center (Anchors) found by Kmean: ")
    print(kmeans.cluster_centers_)


if __name__ == '__main__':
    main()
