import os

from model import YOLO


def main():
    train_image_dir = "D:/data/bdd100k/preprocess_images/train"
    train_label_dir = "D:/data/bdd100k/preprocess_labels/train"
    val_image_dir = "D:/data/bdd100k/preprocess_images/val"
    val_label_dir = "D:/data/bdd100k/preprocess_labels/val"

    train_label_paths = [os.path.join(train_label_dir, f) for f in os.listdir(train_label_dir)]
    train_image_paths = [os.path.join(train_image_dir, os.path.splitext(f)[0] + '.jpg')
                         for f in os.listdir(train_label_dir)]

    val_image_paths = [os.path.join(val_image_dir, f) for f in os.listdir(val_image_dir)]
    val_label_paths = [os.path.join(val_label_dir, f) for f in os.listdir(val_label_dir)]

    model = YOLO()

    w = './weights'
    for d in os.listdir(w):
        f = os.path.join(w, d, 'yolo.ckpt')
        model.restore_weight_test(f)
        model.eval(train_image_paths, train_label_paths, val_image_paths, val_label_paths, batch_size=20)


if __name__ == '__main__':
    main()
