#YOLOv2 Road Object Detection
----
## Cài đặt
- Python 3.6.5
- Các packages có trong requirements.txt

## Sử dụng
Chạy file app.py:
```
python app.py
```
Sau đó truy cập vào http://localhost:5000/ để upload ảnh và xem kết quả.
