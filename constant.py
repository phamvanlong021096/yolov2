# data using is: Berkeley DeepDrive 100k

SCALE = 32
GRID_W = 20
GRID_H = 12
NUM_CLASS = 10
NUM_ANCHOR = 5
IMAGE_W = GRID_W * SCALE  # 640
IMAGE_H = GRID_H * SCALE  # 384
IMAGE_C = 3
ORG_IMAGE_W = 1280
ORG_IMAGE_H = 720
IOU_THRES = 0.7
ALPHA = 0.1
PROB_TH, IOU_TH = 0.7, 0.5

LAMBDA_COORD = 5.0
LAMBDA_NO_OBJ = 0.5
LEARNING_RATE = 0.0001
WEIGHT_PATH = './weights/yolo.ckpt'

ANCHORS = [
    (11.269039, 12.191969),
    (33.36903, 32.414665),
    (75.23372, 62.40968),
    (136.84247, 114.9139),
    (218.38968, 197.9047)
]

CLASSES = {
    'bike': 0,
    'bus': 1,
    'car': 2,
    'motor': 3,
    'person': 4,
    'rider': 5,
    'traffic light': 6,
    'traffic sign': 7,
    'train': 8,
    'truck': 9
}

CATEGORIES = ['bike', 'bus', 'car', 'motor', 'person', 'rider', 'traffic light', 'traffic sign', 'train', 'truck']

COLORS = [(255, 0, 130),
          (255, 255, 0),
          (0, 0, 255),
          (0, 100, 255),
          (255, 50, 0),
          (100, 0, 100),
          (255, 0, 255),
          (168, 0, 0),
          (0, 255, 0),
          (0, 255, 255)]
