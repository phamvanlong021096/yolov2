import json


def main():
    path = 'D:/data/bdd100k/labels/bdd100k_labels_images_train.json'

    categories = []
    with open(path) as file:
        data = json.load(file)

    for d in data:
        labels = d['labels']
        for l in labels:
            if 'box2d' in l:
                c = l['category']
                if c not in categories:
                    categories.append(c)

    categories = sorted(categories)
    print(categories)


if __name__ == '__main__':
    main()
