import cv2
import random
import numpy as np

from constant import *


def shuffle_data(image_paths, label_paths):
    array = []
    n = len(image_paths)
    for i in range(n):
        array.append((image_paths[i], label_paths[i]))
    random.shuffle(array)
    image_paths = [a[0] for a in array]
    label_paths = [a[1] for a in array]

    return image_paths, label_paths


def read_images(paths):
    images = []
    for p in paths:
        image = cv2.imread(p)
        image = (image / 255.0) * 2.0 - 1.0
        images.append(image.astype(np.float32))

    return np.asarray(images)


def read_labels(paths):
    labels = []
    for p in paths:
        label = np.load(p)
        labels.append(label)

    return np.asarray(labels)


def read_image_predict(path):
    image = cv2.imread(path)
    image = cv2.resize(image, (IMAGE_W, IMAGE_H))
    image = (image / 255.0) * 2.0 - 1.0
    image = np.asarray([image.astype(np.float32)])

    return image


def softmax(x):
    e_x = np.exp(x)
    return e_x / np.sum(e_x)


def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))


def iou(r1, r2):
    intersect_w = np.maximum(np.minimum(r1[0] + r1[2], r2[0] + r2[2]) - np.maximum(r1[0], r2[0]), 0)
    intersect_h = np.maximum(np.minimum(r1[1] + r1[3], r2[1] + r2[3]) - np.maximum(r1[1], r2[1]), 0)
    area_r1 = r1[2] * r1[3]
    area_r2 = r2[2] * r2[3]
    intersect = intersect_w * intersect_h
    union = area_r1 + area_r2 - intersect

    return intersect / union
